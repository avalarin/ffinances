//= require knockout
//= require chosen-jquery
//= require knockout-chosen
//= require controls/page
//= require modules/http

$(function() {
  $('#wallet_currency_id, #wallet_type_id').chosen()
})